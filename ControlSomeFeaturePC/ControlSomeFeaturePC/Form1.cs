﻿using Microsoft.Win32;
using Microsoft.WindowsAPICodePack.Net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Management;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using Application = System.Windows.Forms.Application;
using Button = System.Windows.Forms.Button;
using Label = System.Windows.Forms.Label;
using MessageBox = System.Windows.Forms.MessageBox;
using TextBox = System.Windows.Forms.TextBox;
using Timer = System.Windows.Forms.Timer;

namespace ControlSomeFeaturePC
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

        }

        private void btnCDRom_Click(object sender, EventArgs e)
        {
            bool ret = ShowDialog("Please enter the password to change the settings", "Control PC Application");
            if (true == ret)
            {
                if (btnCDRom.Text == "ON")
                {
                    //ToggleCDRom(false);
                    EnableCDRom(false);
                }
                else
                {
                    //ToggleCDRom(true);
                    EnableCDRom(true);
                }
                UpdateGroupPolicy();
            }
            
        }

        private void ReadCDRom()
        {
            try
            {
                using (RegistryKey key = Registry.LocalMachine.CreateSubKey("SOFTWARE\\Policies\\Microsoft\\Windows\\RemovableStorageDevices\\{53f56308-b6bf-11d0-94f2-00a0c91efb8b}", true))
                {
                    if (key != null)
                    {
                        var obj = key.GetValue("Deny_Read");
                        if (obj == null)
                        {
                            btnCDRom.Text = "ON";
                            MessageBox.Show("No config for CD", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            string sStart = obj.ToString();
                            if (sStart == "1")
                            {
                                btnCDRom.Text = "OFF";
                            }
                            else if (sStart == "0")
                            {
                                btnCDRom.Text = "ON";
                            }
                            else
                            {
                                MessageBox.Show("Error detect value CD-Rom", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)  //just for demonstration...it's always best to handle specific exceptions
            {
                //react appropriately
            }
        }

        private void ReadUsb()
        {
            try
            {
                using (RegistryKey key = Registry.LocalMachine.OpenSubKey("SYSTEM\\CurrentControlSet\\Services\\USBSTOR"))
                {
                    if (key != null)
                    {
                        var obj = key.GetValue("Start");
                        string sStart = obj.ToString();
                        if (sStart == "3")
                        {
                            btnUsb.Text = "ON";
                        }
                        else if (sStart == "4")
                        {
                            btnUsb.Text = "OFF";

                        }
                        else
                        {
                            MessageBox.Show("Error detect Value USB", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
            }
            catch (Exception ex)  //just for demonstration...it's always best to handle specific exceptions
            {
                //react appropriately
            }
        }
        private void ReadCamera()
        {
            try
            {
                using (RegistryKey key = Registry.CurrentUser.CreateSubKey("Software\\Microsoft\\Windows\\CurrentVersion\\CapabilityAccessManager\\ConsentStore\\webcam"))
                {
                    if (key != null)
                    {
                        var obj = key.GetValue("Value");
                        string sStart = obj.ToString();
                        if (sStart == "Allow")
                        {
                            btnCamera.Text = "ON";
                        }
                        else if (sStart == "Deny")
                        {
                            btnCamera.Text = "OFF";

                        }
                        else
                        {
                            MessageBox.Show("Error detect Value Camera", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
            }
            catch (Exception ex)  //just for demonstration...it's always best to handle specific exceptions
            {
                //react appropriately
            }
        }

        private void ReadBluetooth()
        {
            try
            {
                using (RegistryKey key = Registry.LocalMachine.CreateSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\ActionCenter\\Quick Actions\\All\\SystemSettings_Device_BluetoothQuickAction", true))
                {
                    if (key != null)
                    {
                        var obj = key.GetValue("Type");
                        string sStart = obj.ToString();
                        if (sStart == "0")
                        {
                            btnBlue.Text = "ON";
                        }
                        else if (sStart == "1")
                        {
                            btnBlue.Text = "OFF";

                        }
                        else
                        {
                            MessageBox.Show("Error detect Value Bluetooth", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
            }
            catch (Exception ex)  //just for demonstration...it's always best to handle specific exceptions
            {
                //react appropriately
            }
        }
        private void ReadNetwork()
        {

            foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (nic.Name.Contains("Ethernet"))//is Ethernet Adapter
                {
                    //ON
                    btnNetwork.Text = "ON";
                    break;
                }
                else
                {
                    btnNetwork.Text = "OFF";
                }
            }
        }

        private void ToggleCDRom(bool value)
        {
            RegistryKey key = Registry.LocalMachine.OpenSubKey("SYSTEM\\CurrentControlSet\\Services\\cdrom", true);
            if (value)
            {
                key.SetValue("Start", 1, RegistryValueKind.DWord);
                btnCDRom.Text = "ON";
            }
            else
            {
                key.SetValue("Start", 4, RegistryValueKind.DWord);
                btnCDRom.Text = "OFF";
            }
            key.Close();
            //reset
            DialogResult dialogResult = MessageBox.Show("Reset PC now?", "Successfully", MessageBoxButtons.OKCancel);
            if (dialogResult == DialogResult.OK)
            {
                ResetPC();
            }
            else if (dialogResult == DialogResult.Cancel)
            {
                //do something else
            }
        }

        private void ToggleUSB(bool value)
        {
            RegistryKey key = Registry.LocalMachine.OpenSubKey("SYSTEM\\CurrentControlSet\\Services\\UsbStor", true);
            if (value)
            {
                key.SetValue("Start", 3, RegistryValueKind.DWord);
                btnUsb.Text = "ON";
            }
            else
            {
                key.SetValue("Start", 4, RegistryValueKind.DWord);
                btnUsb.Text = "OFF";
            }
            key.Close();
        }

        private void ToggleCamera(bool value)
        {
            RegistryKey key = Registry.CurrentUser.OpenSubKey("Software\\Microsoft\\Windows\\CurrentVersion\\CapabilityAccessManager\\ConsentStore\\webcam", true);

            if (value)
            {
                key.SetValue("Value","Allow", RegistryValueKind.String);
                btnCamera.Text = "ON";
               
            }
            else
            {
                key.SetValue("Value", "Deny", RegistryValueKind.String);
                btnCamera.Text = "OFF";
            }
            key.Close();
            //reset
            //DialogResult dialogResult = MessageBox.Show("Reset PC now?", "Successfully", MessageBoxButtons.OKCancel);
            //if (dialogResult == DialogResult.OK)
            //{
            //    ResetPC();
            //}
            //else if (dialogResult == DialogResult.Cancel)
            //{
            //    //do something else
            //}
        }

        private void ToggleBluetooth(bool value)
        {
            RegistryKey key = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\ActionCenter\\Quick Actions\\All\\SystemSettings_Device_BluetoothQuickAction", true);

            if (value)
            {
                key.SetValue("Type", 0, RegistryValueKind.DWord);
                btnBlue.Text = "ON";

            }
            else
            {
                key.SetValue("Type", 1, RegistryValueKind.DWord);
                btnBlue.Text = "OFF";
            }
            key.Close();
            //reset
            //DialogResult dialogResult = MessageBox.Show("Reset PC now?", "Successfully", MessageBoxButtons.OKCancel);
            //if (dialogResult == DialogResult.OK)
            //{
            //    ResetPC();
            //}
            //else if (dialogResult == DialogResult.Cancel)
            //{
            //    //do something else
            //}
        }

        private void ToggleNetwork(bool isEnable = true)
        {
            //string interfaceName = "Local Area Connection";

            //if (value)
            //{
            //    ProcessStartInfo psi = new ProcessStartInfo("netsh", "interface set interface \"" + interfaceName + "\" enable");
            //    Process p = new Process();
            //    p.StartInfo = psi;
            //    p.Start();
            //    btnNetwork.Text = "ON";

            //}
            //else
            //{
            //    ProcessStartInfo psi = new ProcessStartInfo("netsh", "interface set interface \"" + interfaceName + "\" disable");
            //    Process p = new Process();
            //    p.StartInfo = psi;
            //    p.Start();
            //    btnNetwork.Text = "OFF";
            //}

            string interfaceName = "Ethernet";
            string control;
            if (isEnable)
            {
                control = "enable";
                btnNetwork.Text = "ON";
            }
            else
            {
                control = "disable";
                btnNetwork.Text = "OFF";
            }
            ProcessStartInfo psi = new ProcessStartInfo("netsh", "interface set interface \"" + interfaceName + "\" " + control);
            Process p = new Process();
            p.StartInfo = psi;
            p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            p.Start();
            p.WaitForExit();

        }

        private void ResetPC()
        {
            Process.Start("shutdown", "/r /t 0");
        }
        private void ShutDownPC()
        {
            Process.Start("shutdown", "/s /t 0");

        }

        private void btnNetwork_Click(object sender, EventArgs e)
        {
            bool ret = ShowDialog("Please enter the password to change the settings", "Control PC Application");
            if (true == ret)
            {
                if (btnNetwork.Text == "ON")
                {
                    ToggleNetwork(false);
                }
                else
                {
                    ToggleNetwork(true);
                }
            }
            
        }

        private void btnUsb_Click(object sender, EventArgs e)
        {
            //solution 1: change value usbstor 3(on) <-> 4(off) in regedit
            //if (btnUsb.Text == "ON")
            //{
            //    ToggleUSB(false);
            //}
            //else
            //{
            //    ToggleUSB(true);
            //}

            //solution 2: block all removable device by key in regedit 
            bool ret = ShowDialog("Please enter the password to change the settings", "Control PC Application");
            if (true == ret)
            {
                if (btnUsb.Text == "ON")
                {
                    //ToggleUSB(false);
                    EnableRemovableDisk(false);
                    EnableCustomClasses(false);
                    EnableFloppyDrives(false);
                    EnableTapeDrives(false);
                    EnableWPDDevices(false);
                }
                else
                {
                    //ToggleUSB(true);
                    EnableRemovableDisk(true);
                    EnableCustomClasses(true);
                    EnableFloppyDrives(true);
                    EnableTapeDrives(true);
                    EnableWPDDevices(true);
                }
                //DialogResult dialogResult = MessageBox.Show("Reset PC now?", "Successfully", MessageBoxButtons.OKCancel);
                //if (dialogResult == DialogResult.OK)
                //{
                //    ResetPC();
                //}
                //else if (dialogResult == DialogResult.Cancel)
                //{
                //    //do something else
                //}

                UpdateGroupPolicy();
            }
            
        }
        private void ReadRemovableDeivces()
        {
            try
            {
                using (RegistryKey key = Registry.LocalMachine.CreateSubKey("SOFTWARE\\Policies\\Microsoft\\Windows\\RemovableStorageDevices\\{53f5630d-b6bf-11d0-94f2-00a0c91efb8b}",true))
                {
                    if (key != null)
                    {
                        var obj = key.GetValue("Deny_Read");
                        if(obj == null)
                        {
                            btnUsb.Text = "ON";
                            MessageBox.Show("No config for RemovableDeivces", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            string sStart = obj.ToString();
                            if (sStart == "1")
                            {
                                btnUsb.Text = "OFF";
                            }
                            else if (sStart == "0")
                            {
                                btnUsb.Text = "ON";
                            }
                            else
                            {
                                MessageBox.Show("Error detect value all device", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)  //just for demonstration...it's always best to handle specific exceptions
            {
                //react appropriately
            }
        }

        private void ToggleRemovableDeivces(bool value)
        {
            RegistryKey key = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Policies\\Microsoft\\Windows\\RemovableStorageDevices", true);
            if (value)
            {
                key.SetValue("Deny_All", 0, RegistryValueKind.DWord);
                btnUsb.Text = "ON";
            }
            else
            {
                key.SetValue("Deny_All", 1, RegistryValueKind.DWord);
                btnUsb.Text = "OFF";
            }
            key.Close();
        }

        private void btnCamera_Click(object sender, EventArgs e)
        {
           
            bool ret = ShowDialog("Please enter the password to change the settings", "Control PC Application");
            if (true == ret)
            {
                if (btnCamera.Text == "ON")
                {
                    ToggleCamera(false);
                }
                else
                {
                    ToggleCamera(true);
                }
            }
        }


        private void btnBlue_Click(object sender, EventArgs e)
        {
            bool ret = ShowDialog("Please enter the password to change the settings", "Control PC Application");
            if (true == ret)
            {
                if (btnBlue.Text == "ON")
                {
                    ToggleBluetooth(false);
                }
                else
                {
                    ToggleBluetooth(true);
                }
                UpdateGroupPolicy();
            }
            
        }

        private void EnableCDRom(bool value)
        {
            RegistryKey key = Registry.LocalMachine.CreateSubKey("SOFTWARE\\Policies\\Microsoft\\Windows\\RemovableStorageDevices\\{53f56308-b6bf-11d0-94f2-00a0c91efb8b}", true);
            if (value)
            {
                key.SetValue("Deny_Read", 0, RegistryValueKind.DWord);
                key.SetValue("Deny_Write", 0, RegistryValueKind.DWord);
                key.SetValue("Deny_Execute", 0, RegistryValueKind.DWord);
                btnCDRom.Text = "ON";
            }
            else
            {
                key.SetValue("Deny_Read", 1, RegistryValueKind.DWord);
                key.SetValue("Deny_Write", 1, RegistryValueKind.DWord);
                key.SetValue("Deny_Execute", 1, RegistryValueKind.DWord);
                btnCDRom.Text = "OFF";
            }
            key.Close();
        }
        private void EnableRemovableDisk(bool value)
        {
            RegistryKey key = Registry.LocalMachine.CreateSubKey("SOFTWARE\\Policies\\Microsoft\\Windows\\RemovableStorageDevices\\{53f5630d-b6bf-11d0-94f2-00a0c91efb8b}", true);
            if (value)
            {
                key.SetValue("Deny_Read", 0, RegistryValueKind.DWord);
                key.SetValue("Deny_Write", 0, RegistryValueKind.DWord);
                key.SetValue("Deny_Execute", 0, RegistryValueKind.DWord);
                btnUsb.Text = "ON";
            }
            else
            {
                key.SetValue("Deny_Read", 1, RegistryValueKind.DWord);
                key.SetValue("Deny_Write", 1, RegistryValueKind.DWord);
                key.SetValue("Deny_Execute", 1, RegistryValueKind.DWord);
                btnUsb.Text = "OFF";
            }
            key.Close();
        }

        private void EnableCustomClasses(bool value)
        {
            RegistryKey key = Registry.LocalMachine.CreateSubKey("SOFTWARE\\Policies\\Microsoft\\Windows\\RemovableStorageDevices\\Custom", true);
            if (value)
            {
                key.SetValue("Deny_Read", 0, RegistryValueKind.DWord);
                key.SetValue("Deny_Write", 0, RegistryValueKind.DWord);
                btnUsb.Text = "ON";
            }
            else
            {
                key.SetValue("Deny_Read", 1, RegistryValueKind.DWord);
                key.SetValue("Deny_Write", 1, RegistryValueKind.DWord);
                btnUsb.Text = "OFF";
            }
            key.Close();
        }

        private void EnableFloppyDrives(bool value)
        {
            RegistryKey key = Registry.LocalMachine.CreateSubKey("SOFTWARE\\Policies\\Microsoft\\Windows\\RemovableStorageDevices\\{53f56311-b6bf-11d0-94f2-00a0c91efb8b}", true);
            if (value)
            {
                key.SetValue("Deny_Read", 0, RegistryValueKind.DWord);
                key.SetValue("Deny_Write", 0, RegistryValueKind.DWord);
                key.SetValue("Deny_Execute", 0, RegistryValueKind.DWord);

                btnUsb.Text = "ON";
            }
            else
            {
                key.SetValue("Deny_Read", 1, RegistryValueKind.DWord);
                key.SetValue("Deny_Write", 1, RegistryValueKind.DWord);
                key.SetValue("Deny_Execute", 1, RegistryValueKind.DWord);

                btnUsb.Text = "OFF";
            }
            key.Close();
        }

        private void EnableTapeDrives(bool value)
        {
            RegistryKey key = Registry.LocalMachine.CreateSubKey("SOFTWARE\\Policies\\Microsoft\\Windows\\RemovableStorageDevices\\{53f5630b-b6bf-11d0-94f2-00a0c91efb8b}", true);
            if (value)
            {
                key.SetValue("Deny_Read", 0, RegistryValueKind.DWord);
                key.SetValue("Deny_Write", 0, RegistryValueKind.DWord);
                key.SetValue("Deny_Execute", 0, RegistryValueKind.DWord);

                btnUsb.Text = "ON";
            }
            else
            {
                key.SetValue("Deny_Read", 1, RegistryValueKind.DWord);
                key.SetValue("Deny_Write", 1, RegistryValueKind.DWord);
                key.SetValue("Deny_Execute", 1, RegistryValueKind.DWord);

                btnUsb.Text = "OFF";
            }
            key.Close();
        }

        private void EnableWPDDevices(bool value)
        {
            RegistryKey key1 = Registry.LocalMachine.CreateSubKey("SOFTWARE\\Policies\\Microsoft\\Windows\\RemovableStorageDevices\\{6AC27878-A6FA-4155-BA85-F98F491D4F33}", true);
            RegistryKey key2 = Registry.LocalMachine.CreateSubKey("SOFTWARE\\Policies\\Microsoft\\Windows\\RemovableStorageDevices\\{F33FDC04-D1AC-4E8E-9A30-19BBD4B108AE}", true);
            if (value)
            {
                key1.SetValue("Deny_Read", 0, RegistryValueKind.DWord);
                key1.SetValue("Deny_Write", 0, RegistryValueKind.DWord);
                key2.SetValue("Deny_Read", 0, RegistryValueKind.DWord);
                key2.SetValue("Deny_Write", 0, RegistryValueKind.DWord);

                btnUsb.Text = "ON";
            }
            else
            {
                key1.SetValue("Deny_Read", 1, RegistryValueKind.DWord);
                key1.SetValue("Deny_Write", 1, RegistryValueKind.DWord);
                key2.SetValue("Deny_Read", 1, RegistryValueKind.DWord);
                key2.SetValue("Deny_Write", 1, RegistryValueKind.DWord);

                btnUsb.Text = "OFF";
            }
            key1.Close();
            key2.Close();
        }

        private void UpdateGroupPolicy()
        {
            FileInfo execFile = new FileInfo("gpupdate.exe");
            Process proc = new Process();
            proc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            proc.StartInfo.FileName = execFile.Name;
            proc.StartInfo.Arguments = "/force";
            proc.Start();
            //Wait for GPUpdate to finish
            while (!proc.HasExited)
            {
                Application.DoEvents();
                Thread.Sleep(100);
            }
            //MessageBox.Show("Update procedure has finished");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.Visible = false;
            this.ShowInTaskbar = false;

            notifyIcon1.BalloonTipTitle = " ";
            notifyIcon1.BalloonTipText = " ";
            notifyIcon1.Text = "Setup PC";
            this.WindowState = FormWindowState.Minimized;
            Timer myTimer = new Timer();
            myTimer.Interval = 5000;
            myTimer.Tick += new EventHandler(myTimer_Tick);
            myTimer.Start();    

            ReadCDRom();
            ReadNetwork();
            //ReadUsb();
            ReadRemovableDeivces();
            ReadBluetooth();
            ReadCamera();
            GetNetworkAdapter();

        }
        private void myTimer_Tick(object sender, EventArgs e)
        {
            if (btnWifi.Text == "ON")
            {
                SetNetworkAdapter("enable");
            }
            else
            {
                SetNetworkAdapter("disable");
            }
        }


        private void btnWifi_Click(object sender, EventArgs e)
        {
            bool ret = ShowDialog("Please enter the password to change the settings", "Control PC Application");
            if (true == ret)
            {
                if (btnWifi.Text == "ON")
                {
                    btnWifi.Text = "OFF";
                }
                else
                {
                    btnWifi.Text = "ON";
                }
            }
        }

        private void GetNetworkAdapter()
        {
            int num = 0;
            ManagementObjectSearcher networkAdapterSearcher = new ManagementObjectSearcher("root\\cimv2", "select * from Win32_NetworkAdapterConfiguration");
            ManagementObjectCollection objectCollection = networkAdapterSearcher.Get();
            num = objectCollection.Count;

            num = NetworkInterface.GetAllNetworkInterfaces().Length;


            foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (nic.Name.Contains("Wi-Fi"))
                {
                    btnWifi.Text = "ON";
                    return;
                }
                else
                {
                    btnWifi.Text = "OFF";
                }
                //if (nic.Name.Contains("Bluetooth"))
                //{
                //    //block wifi
                //    string interfaceName = nic.Name;
                //    string control = "disable";
                //    ProcessStartInfo psi = new ProcessStartInfo("netsh", "interface set interface \"" + interfaceName + "\" " + control);
                //    Process p = new Process();
                //    p.StartInfo = psi;
                //    p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                //    p.Start();
                //    p.WaitForExit();
                //    MessageBox.Show("Bluetooth Detected - Blocked");

                //}
            }
            return;
        }

        

        private void SetNetworkAdapter(string control)
        {
            //int num = 0;
            //ManagementObjectSearcher networkAdapterSearcher = new ManagementObjectSearcher("root\\cimv2", "select * from Win32_NetworkAdapterConfiguration");
            //ManagementObjectCollection objectCollection = networkAdapterSearcher.Get();
            //num = objectCollection.Count;

            //num = NetworkInterface.GetAllNetworkInterfaces().Length;

           
            //foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
            //{
                //if (nic.Name.Contains("Wi-Fi"))
                //{
                    //block wifi
                    //string interfaceName = nic.Name;
                    string interfaceName = "Wi-Fi";
                    //string control = "disable";
                    ProcessStartInfo psi = new ProcessStartInfo("netsh", "interface set interface \"" + interfaceName + "\" " + control);
                    Process p = new Process();
                    p.StartInfo = psi;
                    p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    p.Start();
                    p.WaitForExit();

                    //MessageBox.Show("Wifi Detected - Blocked");
                //}
                //if (nic.Name.Contains("Bluetooth"))
                //{
                //    //block wifi
                //    string interfaceName = nic.Name;
                //    //string control = "disable";
                //    ProcessStartInfo psi = new ProcessStartInfo("netsh", "interface set interface \"" + interfaceName + "\" " + control);
                //    Process p = new Process();
                //    p.StartInfo = psi;
                //    p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                //    p.Start();
                //    p.WaitForExit();
                //    MessageBox.Show("Bluetooth Detected - Blocked");
                    
                //}
            //}
            return;
        }
        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
            //notifyIcon1.Visible = false;
            WindowState = FormWindowState.Normal;
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            if(WindowState == FormWindowState.Minimized)
            {
                this.Hide();
                notifyIcon1.Visible = true;
                notifyIcon1.ShowBalloonTip(1000);
            }
            else if(FormWindowState.Normal == this.WindowState)
            {
                //notifyIcon1.Visible = false;

                //this.Visible = true;
                //this.ShowInTaskbar = true;
            }
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Show();
            //notifyIcon1.Visible = false;
            WindowState = FormWindowState.Normal;
        }
        private void closeToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            //DialogResult dialog = dialog = MessageBox.Show("Do you really want to close the program?", "SomeTitle", MessageBoxButtons.YesNo);
           

            bool ret = ShowDialog("Do you really want to close the program?", "Control PC Application");
            //MessageBox.Show("ret= "+ret);
            if(false == ret)
            {
                e.Cancel = true;
            }
        }

        public bool ShowDialog(string text, string caption)
        {
            bool ret = false;
            Form prompt = new Form();
            prompt.Width = 350;
            prompt.Height = 200;
            prompt.Text = caption;
            Label textLabel = new Label() { Left = 50, Top = 20, Text = text, Width = 300 };
            //NumericUpDown inputBox = new NumericUpDown() { Left = 50, Top = 50, Width = 200 };
            Label textPassword = new Label() { Left = 50, Top = 40, Text = "Password:" };
            TextBox textBox = new TextBox() { Left = 52, Top = 63, Text = "Abcd@" };
            Label textPasswordNote = new Label() { Left = 50, Top = 85, Text = "", ForeColor = Color.Red};
            Button btnOK = new Button() { Text = "Ok", Left = 150, Width = 70, Top =120 };
            
            btnOK.Click += (sender, e) => {
                string pass = textBox.Text;
                if("Abcd@1234" == pass)
                {
                    prompt.Close();
                    ret = true;

                }
                else
                {
                    textPasswordNote.Text = "Wrong Password!";
                    ret = false;
                }
            };

            Button btnCancel = new Button() { Text = "Cancel", Left = 230, Width = 70, Top = 120 };
            btnCancel.Click += (sender, e) => { prompt.Close(); };

            prompt.Controls.Add(btnOK);
            prompt.Controls.Add(btnCancel);
            prompt.Controls.Add(textPassword);
            prompt.Controls.Add(textPasswordNote);
            prompt.Controls.Add(textBox);
            prompt.Controls.Add(textLabel);
            //prompt.Controls.Add(inputBox);
            prompt.ShowDialog();
            return ret;
        }

    }
}
